package main

import (
	"flag"
)

var logEnabled bool
var listsDir string

func init() {

	flag.BoolVar(&logEnabled, "log", false, "if true, enables logging to the console")
	flag.StringVar(&listsDir, "src", "/etc/dansguardian/lists", "path to directory containing the dansguardian rule lists to be parsed for links")
	flag.Parse()
}

func IsLogEnabled() bool {
	return logEnabled
}

func ListsDirectory() string {
	return listsDir
}