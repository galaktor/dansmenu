package main

import (
	"fmt"
	"strings"
	)

func Doc(body string) string {
	return fmt.Sprintf("<!DOCTYPE html>\n<html>\n<body>%s\n</body>\n</html>", body)
}

func Link(s S) string {
	return fmt.Sprintf("<a href=\"%s\" target=\"_blank\">%s</a>", s.Url, s.Title)
}

func ListItems(sites []S) string {
	result := ""
	for _, s := range sites {
		a := Link(s)
		result += fmt.Sprintf("\t<li>%s</li>\n", a)
	}	
	return strings.TrimRight(result, "\n")
}

func List(content string) string {
	return "<ul>\n" + content + "</ul>"
}
