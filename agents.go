package main

import (
	"fmt"
)

type A struct {
	File string
	Out chan []S
}

func (a *A) WaitAndParse() {
	for {
		LogInfo(fmt.Sprintf("file %s agent waiting on chan %v", a.File, &a.Out))
		select {
		case _,ok := <-a.Out:
			LogInfo("entering agent")
			if !ok {
				LogInfo("breaking")
				// TODO: handle channel closed; maybe log
				break
			}


			LogInfo("reading")
			l, err := Lines(a.File)
			if err != nil {
				// TODO: send error to caller via a channel rather than bring down the process
				LogFatal(fmt.Sprintf("Failed to read file '%v': %s", a.File, err.Error()))
			}

			s := Sites(l)
			
			LogInfo("list items")

//			result := ListItems(s)
			LogInfo(fmt.Sprintf("result for %v %v", l, s))

			LogInfo("returning")
			a.Out <- s
		}
	}
}


func StartAgent(file string) *A {
	LogInfo(fmt.Sprintf("file %v", file))
	
	a := &A{file, make(chan []S)}

	go a.WaitAndParse()

	Agents = append(Agents, a)
	LogInfo(fmt.Sprintf("added agent for %s %v", file, Agents))

	return a
}
