package main

import (
	"fmt"
	"strings"
	"os"
	"bufio"
	)

type S struct {
	Title string
	Url   string
}


func Parse(line string) (site S, success bool) {
	if strings.Contains(line, "#link") {
		parts := strings.Split(line, "#link")
		url := fmt.Sprintf("http://www.%s", strings.TrimSpace(parts[0]))
		site = S{Url: url, Title: strings.TrimSpace(parts[1])}
		success = true
	} else {
		site = S{}
		success = false
	}
	return site, success
}

func Sites(lines []string) []S {
	result := []S{}

	for _, line := range lines {
		site, success := Parse(line)
		if success {
			result = append(result, site)
		}
	}

	return result
}

func Lines(path string) (lines []string, err error) {
	file, ferr := os.Open(path)

	if ferr != nil {
		err = ferr
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := fmt.Sprintf("%s\n", scanner.Text())
		lines = append(lines, line)
	}

	return lines, err
}

type byTitle []S
func (s byTitle) Len() int {return len(s)}
func (s byTitle) Swap(i, j int) { s[i], s[j] = s[j], s[i]}
func (s byTitle) Less(i, j int) bool { return s[i].Title < s[j].Title }
