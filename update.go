package main

/*
`TODOs
 - install as service
 - in addition to sitelist, support urllist? maybe take all files and look for #link? wouldn't want to link from exception lists for example
*/

import (
	"path"
	"io/ioutil"
	"fmt"
	"net/http"
	"sort"
	"runtime"
)

var Agents []*A = make([]*A, 0)

var trigger []S = make([]S, 0) // TODO: make const

func HandleRequest(w http.ResponseWriter, r *http.Request) {

	if(r.Method == "GET") {
		// trigger reload of each agent
		LogInfo("triggering agents")
		for _,a := range Agents {
			LogInfo(fmt.Sprintf("triggering agent %v", a))
			a.Out <- trigger
		}

		// gather results
		LogInfo("gathering results")
		result := make([]S, 0)
		for _,a := range Agents {
			result = append(result, (<-a.Out)...)
		}

		sort.Sort(byTitle(result))

		fmt.Fprintf(w, Doc(List(ListItems(result))))
	} else {
		code := 501 // not implemented
		msg := fmt.Sprintf("http method %s not supported. dansmenu only supports http GET method.", r.Method)
		LogInfo(fmt.Sprintf("received unsupported http method %s. responded %v", r.Method, code))
		http.Error(w, msg, code)
	}



}



func main() {

	LogInfo(fmt.Sprintf("setting GOMAXPROCS from %d to %d", runtime.GOMAXPROCS(runtime.NumCPU()), runtime.GOMAXPROCS(0)))

	files, err := ioutil.ReadDir(ListsDirectory())
	if err != nil {
		LogFatal(fmt.Sprintf("error reading files from directory %s: %s", ListsDirectory(), err.Error()))
	}

	for _,file := range files {
		filepath := path.Join(ListsDirectory(), file.Name())
		StartAgent(filepath)
	}

	http.HandleFunc("/", HandleRequest)

	LogFatal(fmt.Sprintf("%s", http.ListenAndServe(":8080", nil)))
}







