package main

import (
	"os"
	"log"
)

var l *log.Logger = log.New(os.Stdout, "dansmenu: ", 0)

func LogInfo(message string) {
	if IsLogEnabled() {
		l.Print(message)
	}
}

func LogFatal(message string) {
	if IsLogEnabled() {
		l.Fatal(message)
	} else {
		panic(message)
	}
}
